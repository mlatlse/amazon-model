# What's this about

How well can a machine learning algorithm predict consumer preference in the hostile conditions of the Internet? Should we worry about predictive power of information we leave online? We try to answer these questions by looking into how the machine generated recommendations depends on the amount of data supplied by the user in a noisy, non-synthetic dataset.

A broader description of the methodology of our project is available in `poster.pdf`.

# Instructions

- Download and untar https://mlatlse.kszk.eu/books_subset.csv.tar.gz into `data/`
- Run `python preprocess.py` with desired parameters
- Run `python model.py` with desired parameters

A generated 8K example subset is contained in this repository for testing.

![Loss over the example 8K subset](figures/loss.png)
